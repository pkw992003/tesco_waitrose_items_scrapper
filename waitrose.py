from requests_html import HTMLSession
session = HTMLSession()

def checkWaitrose():
    file_name = 'waitrose_urls.txt'
    urls = []


    with open(file_name) as f:
        lines = f.readlines()
        for line in lines:
            urls.append(line.replace("\n",''))
        f.close()
    # print(items)


    # fetch_urls = [
    #     'https://www.tesco.com/groceries/en-GB/products/275969803',
    #     'https://www.tesco.com/groceries/en-GB/products/275969826',
    #     'https://www.tesco.com/groceries/en-GB/products/309770323',
    # ]

    # fetch_url = 'https://www.tesco.com/groceries/en-GB/products/275969803'

    for url in urls:
        my_request = session.get(url)
        html = my_request.html
        productName = html.find('#productName', first=True).text
        productPrice = html.find('span[data-test="product-pod-price"]', first=True).text
        productCurrency = productPrice[0]
        productPriceValue = productPrice[1:]

        originalPrice = html.find('span.offerDescription___13aL3 >em',first=True)
        hasOffer = originalPrice != None
        originalPriceValue = originalPrice.text.split()[1][1:] if hasOffer else None

        # print(str(currency) + str(originalPriceValue) if hasOffer else None)
        # productCurrency = html.find('.currency', first=True).text

        # productOffer = html.find('.offer-text', first=True)
        # hasFor = 'for' in productOffer.text if productOffer != None else None
        # productQuantity = productOffer.text.split()[1]  if hasFor == True else None
        # productOfferPrice = (productOffer.text.split()[0] if hasFor == False else productOffer.text.split()[3] ) if productOffer != None else None
        # productOfferRawPrice = productOfferPrice[1:] if productOffer != None else None

        # productOfferDisplayText = str(productQuantity) + ' for ' + str(productOfferPrice) if hasFor == True else productOfferPrice

        # # print(productOfferRawPrice)
        # # print("productPrice: " + productPrice if productOffer != None else None)
        # # print("productOfferRawPrice: " + productOfferPrice if productOffer != None else None)
        

        # reduced = str(round((float(productPrice) - float(productOfferRawPrice))/float(productPrice)*100,2))+"%" if productOffer != None else None
        reducedDisplayText = str(round(((float(originalPriceValue) - (float(productPriceValue))) / float(originalPriceValue))*100,2))+"%" if hasOffer == True else None
        # print(f"originalPriceValue: {originalPriceValue}")
        # print(f"productPriceValue: {productPriceValue}")
        print(f'Product Name: {productName} \t\t Price:{productPrice if not hasOffer else str(productCurrency+originalPriceValue) } \t Offer Price: {productPrice if hasOffer else None} \t Reduced: {reducedDisplayText}')

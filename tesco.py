from requests_html import HTMLSession
session = HTMLSession()

def checkTesco():
    file_name = 'tesco_items.txt'
    items = []


    with open(file_name) as f:
        lines = f.readlines()
        for line in lines:
            items.append(line.replace("\n",''))
        f.close()
    # print(items)


    # fetch_urls = [
    #     'https://www.tesco.com/groceries/en-GB/products/275969803',
    #     'https://www.tesco.com/groceries/en-GB/products/275969826',
    #     'https://www.tesco.com/groceries/en-GB/products/309770323',
    # ]

    # fetch_url = 'https://www.tesco.com/groceries/en-GB/products/275969803'
    tesco_url = 'https://www.tesco.com/groceries/en-GB/products/'

    for item in items:
        my_request = session.get(tesco_url+item)
        html = my_request.html
        productName = html.find('.product-details-tile__title', first=True).text
        productCurrency = html.find('.currency', first=True)
        if  productCurrency == None:
            print(f'Product Name: {productName} \t\t is not available.')
            continue
        else:
            productCurrency = productCurrency.text
        productPrice = html.find('.value', first=True).text

        productOffer = html.find('.offer-text', first=True)
        hasFor = 'for' in productOffer.text if productOffer != None else None
        productQuantity = productOffer.text.split()[1]  if hasFor == True else None
        productOfferPrice = (productOffer.text.split()[0] if hasFor == False else productOffer.text.split()[3] ) if productOffer != None else None
        productOfferRawPrice = productOfferPrice[1:] if productOffer != None else None

        productOfferDisplayText = str(productQuantity) + ' for ' + str(productOfferPrice) if hasFor == True else productOfferPrice

        # print(productOfferRawPrice)
        # print("productPrice: " + productPrice if productOffer != None else None)
        # print("productOfferRawPrice: " + productOfferPrice if productOffer != None else None)
        

        reduced = str(round((float(productPrice) - float(productOfferRawPrice))/float(productPrice)*100,2))+"%" if productOffer != None else None
        reducedDisplayText = str(round((float(productPrice) - (float(productOfferRawPrice) / float(productQuantity)))/float(productPrice)*100,2))+"%" if hasFor == True else reduced

        print(f'Product Name: {productName} \t\t Price:{productCurrency}{productPrice} \t Club Price: {productOfferDisplayText} \t Reduced: {reducedDisplayText}')

